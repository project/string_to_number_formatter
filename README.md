# String to Number Formatter

This module provides a field formatter that converts a string into a number between 0 and a predefined max number eg 20. 

It was developed so it can be used in combination with CSS to assign a class to an entity based on string text.
Eg. Setting a map marker colour based on a taxonmy term name or setting a user avatar background based on username.

The number should always be the same for the given string, essentially assigning a number to a string value.

You could use views to add this as a class name on a wrapper element.

## Use with SASS

The following will apply a unique colour to items with a class `string-color--XX` where `XX` is unique based on the
string.

```
$color-key-max: 20;

.string-color {
  @for $i from 1 through $color-key-max {
    $rotate: round((($color-key-max / $i) * 360));
    &--#{$i} {
      color: hue-rotate(#{$rotate}deg);
    }
  }
}
```

## Author

Jeremy Graham @ Doghouse Agency